#!/bin/bash

#########################################################################
# MI Cert Script
# Copyright (C) 2021  ash
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#########################################################################
# 
# SHORT DESCRIPTION:
# Script to prepare authentication cert for Plus Multiinfo API user.
# This is wrapper for openssl which is used in two following steps:
# 1. >>Generate .csr<< to generate user private key along with cert signing 
# request (CSR file) which is necessary to obtain signed certificate 
# from Polkomtel. 
# 2. >>Convert .zip<< to convert received zip file to pem file containing 
# user private key, full certification chain and signed user certificate
# which can be reuse in your production environment using Multiinfo API calls.  
#
#########################################################################

echo " __  __ ___    ____ _____ ____ _____                 _       _   
|  \/  |_ _|  / ___| ____|  _ \_   _|  ___  ___ _ __(_)_ __ | |_ 
| |\/| || |  | |   |  _| | |_) || |   / __|/ __| '__| | '_ \| __|
| |  | || |  | |___| |___|  _ < | |   \__ \ (__| |  | | |_) | |_ 
|_|  |_|___|  \____|_____|_| \_\|_|   |___/\___|_|  |_| .__/ \__|
                                                      |_|        
"
echo "...(C)2021 by ash..."
echo

# Check if openssl exist
command -v openssl >/dev/null 2>&1 || { echo >&2 "ERROR: Script require openssl, but it's not installed.  Aborting."; exit 1; }
# Check for unzip tool
command -v unzip >/dev/null 2>&1 || { echo >&2 "ERROR: Script require unzip, but it's not installed. Aborting."; exit 1; }
# Main menu
PS3='Please enter your choice: '
options=("Generate .csr" "Convert .zip" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Generate .csr")
            echo "...Preparing CSR request..."
            echo "Input user name for certificate request i.e. user.api":
            read userapi
            echo "...Generate CSR request for $userapi..."
            if openssl req -newkey rsa:2048 -keyout ${userapi}_private.key -out ${userapi}_Request.csr 2>&1; then
                echo
                echo "...Generation CSR request for $userapi ...SUCCESS..."
                if mv ${userapi}_Request.csr ${userapi}_Request.pem 2>&1; then
                    echo "...Renaming .csr file to .pem file ...SUCCESS..."
                    echo
                    echo "Please send email with generated file: ${userapi}_Request.pem to multiinfo.certyfikaty@plus.pl "
                    echo "and wait for reply email zip file containing signed certificate."
                    echo
                    echo "After receiving zip file, save it into the same folder as this script and run the script again. "
                    echo
                else 
                    echo
                    echo "ERROR: renaming .csr to .pem is failed. Please check the error above and rerun the script."
                    exit 1
                fi
            else
                echo 
                echo "ERROR: openssl has some trouble. Please rerun the script."
                exit 1
            fi
            break
            ;;
        "Convert .zip")
            echo
            echo "...Checking for zip file..."
            # Check for zip input file and unpack
            if [ -f 00000*.zip ]; then
                echo
                echo "...Found some zip file ...unpacking..."
                unzip -u -o 00000*.zip
                filename=$(basename 00000*.zip)
                filename="${filename%.*}"
                    # Check if .zip includes right stuff
                    if [ -f ${filename}.cer -a -f ${filename}.p7b ]; then     
                        user=$(openssl x509 -noout -in ${filename}.cer -text | grep Subject: |awk -F 'CN = |, em' '{print $2}')                 
                        if [ ! -z "$user" ]; then
                            echo
                            echo "...Found signed cert for ${user}"
                            echo
                            echo "...Exporting cert chain from p7b file..."
                            openssl pkcs7 -in ${filename}.p7b -print_certs -out chainfromp7b.pem
                            echo
                            echo "...Joining private key, cert chain and signed certificate into p12 file..."
                            # Check if user_private.key exists 
                            if [ -f ${user}_private.key ]; then
                                if openssl pkcs12 -inkey ${user}_private.key -in ${filename}.cer -certfile chainfromp7b.pem -export -out ${user}.p12 2>&1; then
                                    echo
                                    echo "...Converting p12 file into pem file..."
                                    if openssl pkcs12 -in ${user}.p12 -out ${user}.pem 2>&1; then
                                        echo
                                        echo "SUCCESS: Now copy ${user}.pem file from working folder to your MI environment."
                                        echo "REMEMBER to enable MI user ${user} to access API via MI GUI at https://www.multiinfo.plus.pl"
                                        # Generate fingerprint for use in GUI - cut :
                                        fingerprint=$((openssl x509 -noout -fingerprint -in ${user}.pem)|awk -F 't=' '{print $2}'|tr -d :)
                                        echo "Your pem fingerprint is: ${fingerprint}"
                                        echo
                                        exit 1
                                    else
                                        echo
                                        echo "ERROR: Can not process .p12 file."
                                        echo "Verify PEM pass phrase, left Import Password blank."
                                        echo
                                        exit 1
                                    fi
                                else
                                    echo
                                    echo "ERROR: Probably can't process private key file. "
                                    echo "Verify your pass phrase, Export Password is not required. "
                                    echo
                                    exit 1
                                fi
                            else
                                echo
                                echo "ERROR: Can't find ${user}_private.key file. Please first generate .cer file and private key by using option 1 from menu."
                                echo
                                exit 1
                            fi
                        else
                            echo
                            echo "ERROR: Something wrong with unpacked .cer file. Please check zip file for inconsistency."
                            exit 1	 
                        fi
                    else
                        echo
                        echo "ERROR: Unpacked files don't match. Please check zip file for inconsistency."
                        exit 1
                    fi
            else
                echo
                echo "ERROR: Script require zip file with .p7b and .cer files inside. "
                echo "Please copy it into current directory: ${PWD} "
                echo
            fi
            break
            ;;

        "Quit")
            break
            ;;
        *) echo "ERROR: Invalid option $REPLY";;
    esac
done