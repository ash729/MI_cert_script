## **INTRODUCTION**
This bash script main function is to prepare authentication cert for Plus Multiinfo API user.
This is wrapper for openssl which is used in two following steps:
1) **Generate .csr** to generate user private key along with cert signing request (CSR file) which is necessary to obtain signed 
certificate from Plus (Polkomtel). 
2) **Convert .zip** to convert received zip file to .pem file containing: user private key, full certification chain and signed user 
certificate, which can be reused in your production environment using Multiinfo API calls.  

## **FIRST RUN**
`cd` into working dir.  
Copy script or clone this repo into working dir and `chmod +x` it.  
Run `./mi_cert_script.sh` without additional arguments.  
Choose option 1 (Generate...) to generate file to be emailed to Plus.
Email userapi_Request.pem to multiinfo.certyfikaty@plus.pl  
After receiving .zip file from Plus, save it into the same working dir where the script and private key are.  
Run the script again and choose option 2 (Convert...).  
After successful conversion, grab .pem file from working dir and use it in your target environment. Remember to enable MI API user to access API via MI GUI.  
Login into MI GUI, go to Adminsitracja-Uzytkownicy, edit properties of API user: set him proper permissions for API module and save details of the signed certificate along with proper cert fingerprint. For more details see [MI documentation](https://www.multiinfo.plus.pl/Settings/Documents.aspx) (after logging to MI GUI).    

Enjoy!

